import React from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';
import Colors from '../constants/colors';
import Title from '../components/ui/Title';
import PrimaryButton from '../components/ui/PrimaryButton';

function GameOverScreen({roundsNumber, userNumber, onStartNewGame}) {
  return (
    <View style={styles.rootContainer}>
      <Title>GAME OVER</Title>
      <View style={styles.imageContainer}>
        <Image
          style={styles.imageStyle}
          source={require('../assets/success.jpg')}
        />
      </View>
      <Text style={styles.summaryText}>
        Your phone neended <Text style={styles.hightLight}>{roundsNumber}</Text>{' '}
        round to guess the number{' '}
        <Text style={styles.hightLight}>{userNumber}</Text>.
      </Text>
      <PrimaryButton onPress={onStartNewGame}>Start New Game.</PrimaryButton>
    </View>
  );
}

export default GameOverScreen;
const styles = StyleSheet.create({
  rootContainer: {
    flex: 1,
    padding: 24,
    justifyContent: 'center',
    alignContent: 'center',
  },
  imageContainer: {
    width: 300,
    height: 300,
    borderRadius: 150,
    borderWidth: 3,
    borderColor: Colors.primary500,
    overflow: 'hidden',
    margin: 36,
  },
  imageStyle: {
    width: '100%',
    height: '100%',
  },
  summaryText: {
    fontSize: 24,
    textAlign: 'center',
    marginVertical: 24,
  },
  hightLight: {
    fontSize: 24,
    fontWeight: 'bold',
    color: Colors.primary700,
  },
});
