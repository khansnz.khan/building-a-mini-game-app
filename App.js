import {useState} from 'react';
import {View, Text, StyleSheet, ImageBackground,SafeAreaView} from 'react-native';
import StartGameScreen from './screens/StartGameScreen';
import GameScreen from './screens/GameScreen';
import Colors from './constants/colors';
import GameOverScreen from './screens/GameOverScreen';

export default function App() {
  const [userNumber,setUserNumber] = useState();
  const [gameIsOver,setGameIsOver] = useState(true)
  const [guessRounds,setGuessRounds] = useState(0)

  function pickedNumberHandler(pickedNumber){
    setUserNumber(pickedNumber)
    setGameIsOver(false)
  }
  function gameIsOverHandler(numberOfROunds){
    setGameIsOver(true)
    setGuessRounds(numberOfROunds)
  }
  function startNewGameHandler(){
    setUserNumber(null)
    setGuessRounds(0)
  }
  let screen = <StartGameScreen onPickNumber={pickedNumberHandler}/>
  if(userNumber){
    screen = <GameScreen  userNumber={userNumber} onGameOver = {gameIsOverHandler}/>
  }

  if (gameIsOver && userNumber){
    screen = <GameOverScreen onStartNewGame={startNewGameHandler} roundsNumber={guessRounds} userNumber={userNumber}/>
  }

 
  return (
    <ImageBackground
      style={styles.rootScreen}
      source={require('./assets/backgroundImage.jpg')}
      resizeMode="cover"
      imageStyle={styles.backgroundImage}>
        <SafeAreaView style={styles.rootScreen}>
        {screen}
        </SafeAreaView>
    </ImageBackground>
  );
}
const styles = StyleSheet.create({
  rootScreen: {
    flex: 1,
    // backgroundColor:Colors.accent600
  },
  backgroundImage:{
    opacity:0.15
  }
});
