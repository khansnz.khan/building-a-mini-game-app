const Colors={
    primary500 : '#72063c',
    primary600 : '#640233',
    primary700 : '#ffdab9',
    accent500 : '#ddb52f',
    accent600 : '#afeeee'
}
export default Colors